//Activity
//Design a data model using the same syntax we used in d1 for your upcoming capstone 2 project, which will be an ecommerce website.

//Specify at least the following collections:
//-Users 
//-Products

//Make sure all possible and necessary relationships between data is properly established

//Once done, we will present our work to the class.


//eCommerce Website

//-Products
	//Product Specifics:
//-ID
	//Data type: MongoDB ID
//-Date of Manufacture
	//Data type: Date
//-Date of Expiry
	//Data type: Date
//-Name of product
	//Data type: String
//-Type of product
	//Data type: String

//-Users
	//User Specifics:
//-ID
	//Data type: MongoDB ID
//-Username
	//Data type: String
//-Password
	//Data type: String
//-Email
	//Data type: String
//-Phone Number 
	//Data type: Telephone Number
//-Shipping Address
	//Data type: String
//-Billing Address 
	//Data type: String
//-Language Option
	//Data type: String
//Payment Option 
	//Data type: String

